defmodule PetClinic.AppointmentService.ExpertSchedule do
  @moduledoc """
  Defines the schema of a expert schedule
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "expert_schedules" do
    field(:end_hour, :time)
    field(:start_hour, :time)

    belongs_to(:pet_health_expert, PetClinic.PetClinicService.PetHealthExpert)

    timestamps()
  end

  @doc false
  def changeset(expert_schedule, attrs) do
    expert_schedule
    |> cast(attrs, [:start_hour, :end_hour, :pet_health_expert_id])
    |> validate_required([:start_hour, :end_hour, :pet_health_expert_id])
  end
end
