defmodule PetClinic.AppointmentServiceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PetClinic.AppointmentService` context.
  """

  import PetClinic.PetClinicServiceFixtures

  @doc """
  Generate a appointment.
  """
  def appointment_fixture(attrs \\ %{}) do
    pet = pet_fixture()
    pet_health_expert = pet_health_expert_fixture()

    {:ok, appointment} =
      attrs
      |> Enum.into(%{
        datetime: ~U[2022-04-26 01:58:00Z],
        pet_id: pet.id,
        pet_health_expert_id: pet_health_expert.id
      })
      |> PetClinic.AppointmentService.create_appointment()

    appointment
  end

  @doc """
  Generate a expert_schedule.
  """
  def expert_schedule_fixture(attrs \\ %{}) do
    pet_health_expert = pet_health_expert_fixture()

    {:ok, expert_schedule} =
      attrs
      |> Enum.into(%{
        pet_health_expert_id: pet_health_expert.id,
        start_hour: ~T[08:00:00],
        end_hour: ~T[16:00:00]
      })
      |> PetClinic.AppointmentService.create_expert_schedule()

    expert_schedule
  end
end
