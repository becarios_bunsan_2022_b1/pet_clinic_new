defmodule PetClinic.AppointmentServiceTest do
  use PetClinic.DataCase

  alias PetClinic.AppointmentService

  describe "appointments" do
    alias PetClinic.AppointmentService.Appointment

    import PetClinic.AppointmentServiceFixtures
    import PetClinic.PetClinicServiceFixtures

    @invalid_attrs %{datetime: nil}

    test "list_appointments/0 returns all appointments" do
      appointment = appointment_fixture()
      assert AppointmentService.list_appointments() == [appointment]
    end

    test "get_appointment!/1 returns the appointment with given id" do
      appointment = appointment_fixture()
      assert AppointmentService.get_appointment!(appointment.id) == appointment
    end

    test "create_appointment/1 with valid data creates a appointment" do
      valid_attrs = %{datetime: ~U[2022-04-26 01:58:00Z]}

      assert {:ok, %Appointment{} = _appointment} =
               AppointmentService.create_appointment(valid_attrs)
    end

    test "create_appointment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = AppointmentService.create_appointment(@invalid_attrs)
    end

    test "update_appointment/2 with valid data updates the appointment" do
      appointment = appointment_fixture()
      update_attrs = %{}

      assert {:ok, %Appointment{} = _appointment} =
               AppointmentService.update_appointment(appointment, update_attrs)
    end

    test "update_appointment/2 with invalid data returns error changeset" do
      appointment = appointment_fixture()

      assert {:error, %Ecto.Changeset{}} =
               AppointmentService.update_appointment(appointment, @invalid_attrs)

      assert appointment == AppointmentService.get_appointment!(appointment.id)
    end

    test "delete_appointment/1 deletes the appointment" do
      appointment = appointment_fixture()
      assert {:ok, %Appointment{}} = AppointmentService.delete_appointment(appointment)

      assert_raise Ecto.NoResultsError, fn ->
        AppointmentService.get_appointment!(appointment.id)
      end
    end

    test "change_appointment/1 returns a appointment changeset" do
      appointment = appointment_fixture()
      assert %Ecto.Changeset{} = AppointmentService.change_appointment(appointment)
    end

    test "creating an appointment with not existing expert returns an error" do
      pet_health_expert_id = 1
      pet = pet_fixture()
      datetime = ~N[2022-07-07 15:00:00]

      assert {:error, "expert with id #{pet_health_expert_id} doesn't exist"} ==
               AppointmentService.new_appointment(pet_health_expert_id, pet.id, datetime)
    end

    test "creating an appointment with not existing pet returns an error" do
      pet_health_expert = expert_schedule_fixture()
      pet_id = 1
      datetime = ~N[2022-07-07 15:00:00]

      assert {:error, "pet with id #{pet_id} doesn't exist"} ==
               AppointmentService.new_appointment(
                 pet_health_expert.pet_health_expert_id,
                 pet_id,
                 datetime
               )
    end

    test "giving a past datetime returns error" do
      pet_health_expert = expert_schedule_fixture()
      pet = pet_fixture()
      datetime = ~N[2022-05-05 15:00:00]

      assert {:error, "datetime is in the past"} ==
               AppointmentService.new_appointment(
                 pet_health_expert.pet_health_expert_id,
                 pet.id,
                 datetime
               )
    end

    test "valid data creates an appointment" do
      pet_health_expert = expert_schedule_fixture()
      pet = pet_fixture()
      datetime = ~N[2022-07-07 15:00:00]

      assert {:ok, %Appointment{} = _appointment} =
               AppointmentService.new_appointment(
                 pet_health_expert.pet_health_expert_id,
                 pet.id,
                 datetime
               )
    end
  end

  describe "expert_schedules" do
    alias PetClinic.AppointmentService.ExpertSchedule

    import PetClinic.AppointmentServiceFixtures
    import PetClinic.PetClinicServiceFixtures

    @invalid_attrs %{end_hour: nil, start_hour: nil}

    test "list_expert_schedules/0 returns all expert_schedules" do
      # |> IO.inspect()
      expert_schedule = expert_schedule_fixture()
      assert AppointmentService.list_expert_schedules() == [expert_schedule]
    end

    test "get_expert_schedule!/1 returns the expert_schedule with given id" do
      expert_schedule = expert_schedule_fixture()
      assert AppointmentService.get_expert_schedule!(expert_schedule.id) == expert_schedule
    end

    test "create_expert_schedule/1 with valid data creates a expert_schedule" do
      expert_schedule = expert_schedule_fixture()

      valid_attrs = %{
        end_hour: ~T[14:00:00],
        start_hour: ~T[14:00:00],
        pet_health_expert_id: expert_schedule.pet_health_expert_id
      }

      assert {:ok, %ExpertSchedule{} = expert_schedule} =
               AppointmentService.create_expert_schedule(valid_attrs)

      assert expert_schedule.end_hour == ~T[14:00:00]
      assert expert_schedule.start_hour == ~T[14:00:00]
    end

    test "create_expert_schedule/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               AppointmentService.create_expert_schedule(@invalid_attrs)
    end

    test "update_expert_schedule/2 with valid data updates the expert_schedule" do
      expert_schedule = expert_schedule_fixture()
      update_attrs = %{end_hour: ~T[15:01:01], start_hour: ~T[15:01:01]}

      assert {:ok, %ExpertSchedule{} = expert_schedule} =
               AppointmentService.update_expert_schedule(expert_schedule, update_attrs)

      assert expert_schedule.end_hour == ~T[15:01:01]
      assert expert_schedule.start_hour == ~T[15:01:01]
    end

    test "update_expert_schedule/2 with invalid data returns error changeset" do
      expert_schedule = expert_schedule_fixture()

      assert {:error, %Ecto.Changeset{}} =
               AppointmentService.update_expert_schedule(expert_schedule, @invalid_attrs)

      assert expert_schedule == AppointmentService.get_expert_schedule!(expert_schedule.id)
    end

    test "delete_expert_schedule/1 deletes the expert_schedule" do
      expert_schedule = expert_schedule_fixture()
      assert {:ok, %ExpertSchedule{}} = AppointmentService.delete_expert_schedule(expert_schedule)

      assert_raise Ecto.NoResultsError, fn ->
        AppointmentService.get_expert_schedule!(expert_schedule.id)
      end
    end

    test "change_expert_schedule/1 returns a expert_schedule changeset" do
      expert_schedule = expert_schedule_fixture()
      assert %Ecto.Changeset{} = AppointmentService.change_expert_schedule(expert_schedule)
    end

    test "Wrong date ranges returns error" do
      pet_health_expert = expert_schedule_fixture()
      from_date = ~D[2022-04-26]
      to_date = ~D[2022-04-25]

      assert {:error, "wrong date range, are you from the future?"} ==
               AppointmentService.available_slots(
                 pet_health_expert.pet_health_expert_id,
                 from_date,
                 to_date
               )
    end

    test "not existing expert returns an error" do
      pet_health_expert_id = 1
      from_date = ~D[2022-04-25]
      to_date = ~D[2022-04-26]

      assert {:error, "expert with id #{pet_health_expert_id} doesn't exist"} ==
               AppointmentService.available_slots(pet_health_expert_id, from_date, to_date)
    end

    # test "show block hours if data is correct" do
    #   schedule = expert_schedule_fixture()
    #   assert {}
    #          == AppointmentService.available_slots(pet_health_expert_id, from_date, to_date)
    # end

    test "returns a block of hours between two given times" do
      blocks = [
        ~T[08:00:00],
        ~T[08:30:00],
        ~T[09:00:00],
        ~T[09:30:00],
        ~T[10:00:00],
        ~T[10:30:00],
        ~T[11:00:00],
        ~T[11:30:00],
        ~T[12:00:00],
        ~T[12:30:00],
        ~T[13:00:00],
        ~T[13:30:00],
        ~T[14:00:00],
        ~T[14:30:00],
        ~T[15:00:00],
        ~T[15:30:00],
        ~T[16:00:00]
      ]

      start_hour = ~T[08:00:00]
      end_hour = ~T[16:00:00]

      assert blocks == AppointmentService.block_hours(start_hour, end_hour)
    end
  end
end
